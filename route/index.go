package route

import (
	"encoding/json"
	"errors"
	"github.com/gin-gonic/gin"
	"reflect"
	"strings"
)

var RouteDefineCallbackTypeError = errors.New("callback type error when defining route")

type Controller interface {
	BeforeDispatch()
	AfterDispatch()
}

//request method
const (
	POST = iota
	GET
	HEAD
	PUT
)

const MethodUnset RequestMethod = -1

type RequestMethod int

type Route struct {
	Method      RequestMethod //request method
	Namespace   string
	Rules       map[string]string
	Params      map[string]string
	Controller  *Controller
	Action      string
	isCallback  bool
	Middlewares []Middleware
	callback    func(c *gin.Context)
}

func (r *Route) String() string {
	var method string
	if r.Method == GET {
		method = "get"
	} else if r.Method == POST {
		method = "post"
	} else if r.Method == HEAD {
		method = "head"
	} else if r.Method == PUT {
		method = "put"
	}

	var prefix = "method:" + method + " namespace:" + r.Namespace
	if r.Rules != nil {
		rules, err := json.Marshal(r.Rules)
		if err != nil {
			panic(err)
		}
		prefix += " rules:" + string(rules)
	}

	if r.isCallback {
		prefix += " callable"
	} else {
		controllerName := reflect.TypeOf(r.Controller).Name()
		prefix += " controller:" + controllerName + " action:" + r.Action
	}
	mid := " middleware:["
	for i := 0; i < len(r.Middlewares); i++ {
		mid += reflect.TypeOf(r.Middlewares[i]).Elem().Name() + ","
	}
	mid = strings.TrimRight(mid, ",")
	mid += "]"
	prefix += mid

	return prefix
}

type Middleware interface {
	Handle() bool
}

type Router struct {
	Controllers []Controller
	Routes      []Route
}

type Condition struct {
	Namespace   string
	Rules       map[string]string
	Middlewares []Middleware
	Method      RequestMethod
}

type RouteStack struct {
	conditions []Condition //stack
	current    *Condition  //current
}

var GlobalRouter Router

func init() {
}

func (gr *Router) RegisterController(c Controller) {
	gr.Controllers = append(gr.Controllers, c)
}

func (gr *Router) RegisterRoute(r Route) {
	gr.Routes = append(gr.Routes, r)
}

func RouteStackBuild() *RouteStack {
	return &RouteStack{current: nil}
}

func (rsb *RouteStack) CreateCondition() *Condition {
	var lastCondition *Condition = nil
	if len(rsb.conditions) > 0 {
		lastCondition = &rsb.conditions[len(rsb.conditions)-1]
	}
	newCondition := &Condition{Rules: make(map[string]string)}
	if lastCondition != nil {
		newCondition.Rules = lastCondition.Rules
		newCondition.Namespace = lastCondition.Namespace
		newCondition.Middlewares = lastCondition.Middlewares
		newCondition.Method = lastCondition.Method
	}

	return newCondition
}

func (rsb *RouteStack) Namespace(np string) *RouteStack {
	if rsb.current == nil {
		rsb.current = rsb.CreateCondition()
	}
	rsb.current.Namespace = strings.TrimRight(rsb.current.Namespace, "/") + "/" + strings.TrimLeft(np, "/")
	return rsb
}

func (rsb *RouteStack) Rules(rules map[string]string) *RouteStack {
	if rsb.current == nil {
		rsb.current = rsb.CreateCondition()
	}
	for k, v := range rules {
		rsb.current.Rules[k] = v
	}
	return rsb
}

func (rsb *RouteStack) Middleware(middlewares ...Middleware) *RouteStack {
	if rsb.current == nil {
		rsb.current = rsb.CreateCondition()
	}
	rsb.current.Middlewares = append(rsb.current.Middlewares, middlewares...)
	return rsb
}

func (rsb *RouteStack) Method(method RequestMethod) *RouteStack {
	if rsb.current == nil {
		rsb.current = rsb.CreateCondition()
	}
	rsb.current.Method = method
	return rsb
}

func (rsb *RouteStack) ExecuteControllerAction(c Controller, action string) {
	namespace := rsb.current.Namespace
	route := Route{
		Controller:  &c,
		Action:      action,
		isCallback:  false,
		Rules:       rsb.current.Rules,
		Middlewares: rsb.current.Middlewares,
		Namespace:   namespace}
	GlobalRouter.RegisterRoute(route)
	rsb.current=nil
}

func mergeNamespace(rsb *RouteStack) string {
	var namespace string
	if len(rsb.conditions) >= 1 {
		lastCurrent := rsb.conditions[len(rsb.conditions)-1]
		namespace = strings.TrimRight(lastCurrent.Namespace, "/") + "/" + strings.TrimLeft(rsb.current.Namespace, "/")
	} else {
		namespace = rsb.current.Namespace
	}
	return namespace
}

type RouteClosure func()

func (rsb *RouteStack) ExecuteClosure(callback interface{}) {
	var m RequestMethod
	if rsb.current.Method == MethodUnset {
		m = GET
	} else {
		m = rsb.current.Method
	}
	namespace := rsb.current.Namespace
	switch callback.(type) {
	case func(stack *RouteStack):
		handleNestedRoute(rsb, callback.(func(stack *RouteStack)))
	case func(c *gin.Context):
		route := Route{
			Namespace:   namespace,
			Controller:  nil,
			isCallback:  true,
			Rules:       rsb.current.Rules,
			callback:    callback.(func(c *gin.Context)),
			Method:      m,
			Middlewares: rsb.current.Middlewares}
		GlobalRouter.RegisterRoute(route)
		rsb.current=nil
	default:
		panic(RouteDefineCallbackTypeError)
	}
}

func handleNestedRoute(rsb *RouteStack, callback func(stack *RouteStack)) {
	rsb.beforeClosure()
	callback(rsb)
	rsb.AfterClosure()
}

func (rsb *RouteStack) beforeClosure() {
	rsb.conditions = append(rsb.conditions, *rsb.current)
	rsb.current = nil
}

func (rsb *RouteStack) AfterClosure() {
	rsb.current = &rsb.conditions[len(rsb.conditions)-1] //
	rsb.conditions = rsb.conditions[0 : len(rsb.conditions)-1]
}
