package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"pbuf/controllers"
	"pbuf/middlewares"
)
import "pbuf/route"

func main() {
	rules := make(map[string]string)
	rules["id"] = "\\d{1,2}"
	route.RouteStackBuild().Namespace("/index").Rules(rules).Middleware(&middlewares.AuthMiddleware).ExecuteClosure(func(rsb *route.RouteStack) {
		rules := make(map[string]string)
		rules["uid"] = "1"
		rsb.Namespace("second").Middleware(&middlewares.GuestMiddleware).Rules(rules).ExecuteControllerAction(&controllers.IndexController{}, "index")
		rsb.Namespace("three").ExecuteClosure(func(rsb *route.RouteStack) {
			rsb.Method(route.POST).Namespace("inner").ExecuteClosure(func(c *gin.Context) {
				fmt.Println("called")
			})
		})
	})

	route.RouteStackBuild().Namespace("/order").Middleware(&middlewares.AuthMiddleware).ExecuteControllerAction(&controllers.OrderController{}, "add")
	fmt.Println(len(route.GlobalRouter.Routes))
	for i := 0; i < len(route.GlobalRouter.Routes); i++ {
		fmt.Println(route.GlobalRouter.Routes[i].String())
	}
}
